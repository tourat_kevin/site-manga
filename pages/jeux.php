<?php session_start(); ?>

<!DOCTYPE html>
<html lang="fr">

<head>
  <title>MangaGaming by Alex</title>
  <!-- Required meta tags -->
  <meta name="description" content="ceci est la meta description en cours de développement de Manda@land">
  <?php include 'header/stylesheet.php' ?>
</head>

<body id="haut">
<!-- <div id="progressbar"></div>
<div id="scrollPath"></div> -->
  <!-- =========================================================================================================================================================================== -->
  <!-- navbar -->

  <?php include 'header/navBar.php' ?>

  <!-- fin de nav  -->
  <div class="page-header clear-filter" data-parallax="true">
    <div class="container">
      <div class="row">
        <div class="col-md-8 ml-auto mr-auto">
          <div class="brand text-center">
            <h1>MangaGaming</h1>
            <h3 class="title text-center">Site en cours de développement</h3>
            <div class="mouse_wave">
              <span class="scroll_arrows one"></span>
              <span class="scroll_arrows two"></span>
              <span class="scroll_arrows three"></span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="container">
<div class="row">

<?php

require '../bdd.php';

$page = (!empty($_GET['page']) ? $_GET['page'] : 1);
$limite = 24;

// Partie "Requête"
/* On commence par récupérer le nombre d'éléments total. Comme c'est une requête,
 * il ne faut pas oublier qu'on ne récupère pas directement le nombre.
 * Ici, comme la requête ne contient aucune donnée client pour fonctionner,
 * on peut l'exécuter ainsi directement */
$resultFoundRows = $dbh->query('SELECT count(id) FROM `jeux`');
/* On doit extraire le nombre du jeu de résultat */
$nombredElementsTotal = $resultFoundRows->fetchColumn();

$debut = ($page - 1) * $limite;
/* Cette requête ne change pas */
$query = 'SELECT id, title, rating, description, img FROM `jeux` ORDER BY `title` LIMIT :limite OFFSET :debut';
$query = $dbh->prepare($query);
$query->bindValue('debut', $debut, PDO::PARAM_INT);
$query->bindValue('limite', $limite, PDO::PARAM_INT);
$query->execute();

// Partie "Boucle"
while ($e = $query->fetch()) {
    
  echo '<div class="col-md-4 col-card">
          <div class="card-container">
            <div class="card">
                <div class="front">
                  <div class="header-front">
                    <h3>'.$e['title'].'</h3>
                  </div>
                  <div class="space-30"></div>
                  <img src="../'.$e['img'].'?'.rand().'" alt="image-anime">
                </div>
                <div class="back">
                  <div class="header">
                      <h3 class="motto">'.$e['title'].'</h3>
                  </div>
                    <div class="content">
                        <div class="main">
                            <h4 class="text-center">Description</h4>
                            <p class="text-center">'.$e['description'].'</p>

                            <div class="stats-container">
                                <div class="stats">
                                    <h4>Vote</h4>
                                    <h4>'.$e['rating'].'</h4>
                                </div>
                                 <a class="btn btn-outline-info" href="jeuxInfo.php?id='.$e['id'].'">Voir ici</a>
                            </div>

                        </div>
                    </div>
                  
                </div>
              </div>
            </div>
          </div>';
          }          
?>
<div class="space-800"></div>
<?php
// Partie "Liens"
/* On calcule le nombre de pages */
$nombreDePages = ceil($nombredElementsTotal / $limite);

/* Si on est sur la première page, on n'a pas besoin d'afficher de lien
 * vers la précédente. On va donc ne l'afficher que si on est sur une autre
 * page que la première */
if ($page > 1):
    ?><a href="?page=<?php echo $page - 1; ?>">Page précédente</a> — <?php
endif;

/* On va effectuer une boucle autant de fois que l'on a de pages */
for ($i = 1; $i <= $nombreDePages; $i++):
     /*?><a href="?page=<?php echo $i; ?>"><?php echo $i; ?></a> <?php */
endfor;
?>


<?php
/* Avec le nombre total de pages, on peut aussi masquer le lien
 * vers la page suivante quand on est sur la dernière */
if ($page < $nombreDePages):
    ?>— <a href="?page=<?php echo $page + 1; ?>">Page suivante</a><?php
endif;
?>

            </div>
        </div>
    </div>
</div>
<?php include 'footer.php' ?>

<script src="../assets/js/screen.js" type="text/javascript"></script>