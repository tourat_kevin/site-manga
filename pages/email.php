<?php
// $to = 'alexandre.pro3525@gmail.com';
// $subject = $_POST['subject'];
// $message = $_POST['message']; 
// $from = $_POST['email'];

// // Envoi d'email
// if(mail($to, $subject, $message)){
//     header('location: index.php');
//     echo 'Votre message a été envoyé avec succès!';
// } else{
//     echo '<div class="alert error">Impossible d\'envoyer des emails. Veuillez réessayer.</div>';
// }
?>


<?php
    // Initialisation des variables pour récupérer les valeurs entrées dans le formulaire. 
    $email = $subject = $message = "";

    // Déclaration des variables Error pour ces mêmes inputs.
    $emailError = $subjectError = $messageError = "";
    $isSuccess = false;

    // Envoi de l'email :

    $emailTo = "alexandre.pro3525@gmail.com";

  ##############     RECUPERATION DES DONNÉES     ###############
    if($_SERVER["REQUEST_METHOD"] == "POST"){
        $email = verifyInput($_POST['email']);
        $subject = verifyInput($_POST['subject']);
        $message = verifyInput($_POST['message']);
        // Une fois le formulaire envoyé, le isSuccess est true.
        $isSuccess = true;
        $emailText = "";

	#################### LES FONCTIONS : #####################

    // Particularité pour l'email et le téléphone :
    function isEmail($adresseEmail){
        return filter_var($adresseEmail, FILTER_VALIDATE_EMAIL);
    }

    
    // Pensons à la sécurité : 
    function verifyInput($varToVerify){
        // trim() enlève tous les espaces, les tabulations, les retours à la ligne des champs du formulaire.
        $varToVerify = trim($varToVerify);
        // stripcslashes va retirer tous les antislash
        $varToVerify = stripcslashes($varToVerify);
        // Contre les Cross-Site-Scripting (XSS)
        $varToVerify = htmlspecialchars($varToVerify);

        return $varToVerify;
    }

        ############    VALIDATION / VERIFICATION :    ################

        if(!isEmail($email)){
            $emailError = "Are you kidding me ?";
            $isSuccess = false;
        } else {
            $emailText .= "Email : $email\n";
        }

        if(!isPhone($subject)){
            $subjectError = "Incorrect phone number.";
            $isSuccess = false;
        } else {
            $emailText .= "Subject : $subject\n";
        }

        if(empty($_POST['message'])){
            $messageError = "Whaaaaat ?";
            $isSuccess = false;
        } else {
            $emailText .= "Message : $message\n";
        }

        if($isSuccess){
            $headers = "From: <$email>\r\nReply-To: $email";
            mail($emailTo,"Formulaire de contact en PHP", $emailText, $headers);

            // Lorsque l'email a été envoyé, je remets tous mes champs à 0. 
            $email = $subject = $message = "";
        }
    }

?>