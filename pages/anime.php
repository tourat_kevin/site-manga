<?php
require '../bdd.php';


$page = (!empty($_GET['page']) ? $_GET['page'] : 1);
$limite = 24;

// Partie "Requête"
/* On commence par récupérer le nombre d'éléments total. Comme c'est une requête,
 * il ne faut pas oublier qu'on ne récupère pas directement le nombre.
 * Ici, comme la requête ne contient aucune donnée client pour fonctionner,
 * on peut l'exécuter ainsi directement */
$resultFoundRows = $dbh->query('SELECT count(id) FROM `anime`');
/* On doit extraire le nombre du jeu de résultat */
$nombredElementsTotal = $resultFoundRows->fetchColumn();

$debut = ($page - 1) * $limite;
/* Cette requête ne change pas */
$query = 'SELECT id, name, rating, description, img FROM `anime` ORDER BY `name` LIMIT :limite OFFSET :debut';
$query = $dbh->prepare($query);
$query->bindValue('debut', $debut, PDO::PARAM_INT);
$query->bindValue('limite', $limite, PDO::PARAM_INT);
$query->execute();

// Flitre alphabet
$alphabet = array('0-9'); // ligne modifiée
for($i = 'A'; $i != 'AA'; $i++){
    $alphabet[] = $i;
}
echo '<a href="/">#</a>';
foreach($alphabet as $list){
    echo '<a href="index.php?tri='.$list.'">'.$list.'</a> ';
}
echo '<div class="space-100"></div>';
$nombreDePages = ceil($nombredElementsTotal / $limite);
if(isset($_GET['tri']) AND in_array($_GET['tri'], $alphabet)){
    $tri = $_GET['tri'];
    $tri == '0-9' ? $where = "REGEXP '^([0-9]+)'" : $where = "LIKE '$tri%'"; // ligne ajoutée
    $req = $dbh->prepare("SELECT * FROM `anime` WHERE id AND name $where LIMIT :limite OFFSET :debut");  // ligne modifiée
    $req->bindValue('debut', $debut, PDO::PARAM_INT);
    $req->bindValue('limite', $limite, PDO::PARAM_INT);
    $req->execute();
// Boucle anime trier
    while($e = $req->fetch(PDO::FETCH_ASSOC)){
        echo '<div class="col-md-4 col-card">
            <div class="card-container">
                <div class="card">
                    <div class="front">
                    <div class="header-front">
                        <h3>'.$e['name'].'</h3>
                    </div>
                    <div class="space-30"></div>
                    <img src="../'.$e['img'].'?'.rand().'" alt="image-anime">
                    </div>
                    <div class="back">
                    <div class="header">
                        <h3 class="motto">'.$e['name'].'</h3>
                    </div>
                        <div class="content">
                            <div class="main">
                                <h4 class="text-center">Description</h4>
                                <p class="text-center">'.$e['description'].'</p>

                                <div class="stats-container">
                                    <div class="stats">
                                        <h4>Vote</h4>
                                        <h4>'.$e['rating'].'</h4>
                                    </div>
                                    <a class="btn btn-outline-info" href="animeInfo.php?id='.$e['id'].'">Voir ici</a>
                                </div>

                            </div>
                        </div>
                    
                    </div>
                </div>
            </div>
        </div>';
    }

     // "page précédant" pour la boucle trier
     $pageRootFilter = "index.php?tri=$tri";
     $down = $page -1;
     $pageDown = $pageRootFilter."&page=$down";
 
     if ($page > 1):
         ?><a href="<?php echo $pageDown ?>">Page précédente</a> — <?php
     endif;

    // "page suivante" pour la boucle trier
    $pageRootFilter = "index.php?tri=$tri";
    $up = $page +1;
    $pageUp = $pageRootFilter."&page=$up";
    // var_dump($pageUp);
    if ($page < $nombreDePages):
        ?>— <a href="<?php echo $pageUp ?>">Page suivante</a><?php 
    endif;

   
//  var_dump($_SERVER['PHP_SELF']);
}
else{
    $req2 = $dbh->prepare("SELECT * FROM `anime` ORDER BY `name` LIMIT :limite OFFSET :debut");  // ligne modifiée
    $req2->bindValue('debut', $debut, PDO::PARAM_INT);
    $req2->bindValue('limite', $limite, PDO::PARAM_INT);
    $req2->execute();
 
// Boucle quand aucun trie n'est selectionner
    while($e = $req2->fetch(PDO::FETCH_ASSOC)){
        echo '<div class="col-md-4 col-card">
                <div class="card-container">
                <div class="card">
                    <div class="front">
                        <div class="header-front">
                        <h3>'.$e['name'].'</h3>
                        </div>
                        <div class="space-30"></div>
                        <img src="../'.$e['img'].'?'.rand().'" alt="image-anime">
                    </div>
                    <div class="back">
                        <div class="header">
                            <h3 class="motto">'.$e['name'].'</h3>
                        </div>
                        <div class="content">
                            <div class="main">
                                <h4 class="text-center">Description</h4>
                                <p class="text-center">'.$e['description'].'</p>

                                <div class="stats-container">
                                    <div class="stats">
                                        <h4>Vote</h4>
                                        <h4>'.$e['rating'].'</h4>
                                    </div>
                                    <a class="btn btn-outline-info" href="animeInfo.php?id='.$e['id'].'">Voir ici</a>
                                </div>

                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>';
    }

    // "page précédant" pour la boucle non trier
    $down = $page -1;
    $pageDown = "?page=$down";

    if ($page > 1):
        ?><a href="<?php echo $pageDown ?>">Page précédente</a> — <?php
    endif;

    // "page précédant" pour la boucle non trier
    $g = $page +1;
    $pageUp = "?page=$g";
    // var_dump($pageUp);
    if ($page < $nombreDePages):
        ?>— <a href="<?php echo $pageUp ?>">Page suivante</a><?php 
    endif;

    
}