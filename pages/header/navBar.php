 <!-- navbar -->

 <nav class="navbar navbar-img navbar-transparent navbar-color-on-scroll fixed-top navbar-expand-lg" color-on-scroll="15" id="sectionsNav">
    <div class="container">
        <div class="navbar-translate ml-auto">
            <a class="navbar-brand yes-mobile" href="/">
            <img src="../../image/logo.png" alt="logo-pc" class="img nav-gif no-mobile yes-mobile img-nav">           
            </a>            
            <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="navbar-nav ml-auto">              
                <li class="nav-item dropdown">
                    <!-- liste menu -->
                    <li class="nav-item">
                        <a class="nav-link" href="../pages/manga.php">
                            Manga
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../index.php">
                          Animé
                        </a>
                    </li>                   
                    <li class="nav-item">
                        <a class="nav-link" href="./jeux.php">  
                           Jeux
                        </a>
                    </li>                   
                </li>
            </ul>
                    <!-- ma barre de recherche -->
            <ul class="navbar-nav ml-auto">
              <!-- test -->

              <li class="dropdown nav-item" style="margin-right: 300px;">
                    <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" >
                      <i class="material-icons">search</i>Search
                    </a>
                    <div class="dropdown-menu" style="width: 500px; background-color: #201e1f; padding:5px">
                        <form action="../index.php" method="GET">
                          <div class="form-group container">
                            <div class="row">
                            <div class="col-sm-9">
                              <input class="form-control mr-sm-5" type="search" id="search" name="q" placeholder="Search" aria-label="Search" style="color: #09a3fc !important;">
                            </div>
                            <div class="col-sm-3">
                              <button class="btn btn-search" type="submit" name="press-btn-search" value="Valider">
                                <i class="material-icons" style="top: 10px !important; font-size: 24px !important;">search</i>
                              </button>
                            </div></div>
                            <div style="flex-direction: column; margin-top: 20px;">
                              <div class="result-dropdown" id="result-search"></div>
                            </div>
                          </div>
                            
                        </form>
                    </div>
                </li>
            </ul>    
            <ul class="navbar-nav ml-auto">
                    <!-- ma nav déco / reco -->
                    <?php
              if(isset($_SESSION['connect']) == 0) {
              echo '
              <li class="nav-item">
                <a class="nav-link" href="../pages/Account/login.php">
                  <i class="material-icons">login</i>
                  Login
                </a>           
              </li>';
              } else if(isset($_SESSION['connect']) == 1) {
              echo '
              <li class="nav-item dropdown mr-auto" style="cursor: pointer;">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                  <img class="userNavBar" src="../../image/user.png">
                </a>
                <div class="dropdown-menu dropdown-menu-right">';
                if($_SERVER['PHP_SELF'] == '/pages/Account/profil.php' || $_SERVER['PHP_SELF'] == '/pages/Account/edit.php') {
                  echo '<a class="dropdown-item" href="../Account/profil.php?id='.$_SESSION['id'].'">Paramètre</a>
                  <a id="deco" class="nav-link" href="../Account/logout.php">
                    <i class="material-icons">power_settings_new</i>
                  </a>
                </li>';
                } else {
                 echo '<a class="dropdown-item" href="../pages/Account/profil.php?id='.$_SESSION['id'].'">Paramètre</a>
                  <a id="deco" class="nav-link" href="../pages/Account/logout.php">
                    <i class="material-icons">power_settings_new</i>
                  </a>
              </li>';}}
              ?>
            </ul>        
        </div>
    </div>
</nav>