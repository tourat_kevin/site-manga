<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<!--     Fonts and icons     -->
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
<!-- Material Kit CSS -->
<!-- <link href="assets/css/material-kit.css?v=2.0.6" rel="stylesheet" /> -->
<link href="../../assets/css/material-kit-perso.min.css" rel="stylesheet" />
<!-- <link href="../../assets/css/material-kit-copy.css" rel="stylesheet" /> -->

<link href="../../assets/css/mainV2.css" rel="stylesheet" id="theme-link">
<link href="../../assets/css/scroll.css" rel="stylesheet">
<!-- <link href="../assets/css/main.css" rel="stylesheet"> -->
<!-- <script src="../assets/js/white.js"></script> -->

<!-- <link rel="shortcut icon" type="image/png" href="/favicon.png"/> -->