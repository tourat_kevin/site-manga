<?php require 'email.php' ?>
<form class="form-contact" method="POST" action="email.php" <?php if($_SERVER['PHP_SELF'] == '/pages/Account/profil.php' || $_SERVER['PHP_SELF'] == '/pages/Account/edit.php' || $_SERVER['PHP_SELF'] == '/pages/test/form_update_bdd.php') {echo 'style="display: none;"';} ?>>
    <input class="form-input" type="email" name="email" placeholder="Votre email" value="<?php echo $email ; ?>">
    <p class="comments"><?php echo $emailError;?></p>
    <input class="form-input" type="text" name="subject" placeholder="Sujet" value="<?php echo $subject ; ?>">
    <p class="comments"><?php echo $subjectError;?></p>
    <textarea class="form-input" name="#" id="#" name="message" placeholder="Votre message" cols="30" rows="5" value=""></textarea>
    <p class="comments"><?php echo $messageError;?></p>
    <div class="space-30"></div>
    <button class="btn btn-outline-info" type="submit" value="Envoyer">Envoyer</button>
</form>
<div class="footer1 footer-default" id="footer">
    <div class="btncookie" id="boxcookie"> <!-- bannière cookie -->
        <p class="p-cookie">Nous utilisons des cookies pour vous garantir une meilleure expérience et améliorer la performance de notre site.</p>
        <p>MangaGaming certifie de ne pas vendre vos données personelles.</p>     
        <button id="nocookie" type="button" class="btn btn-outline-info">Non</button>
        <button id="okcookie" type="button" class="btn btn-outline-info">Oui</button>   
    </div>
</div>
<footer>
<div class="rgdp text-center-item">
    <ul>
        <li><a target="_blank" rel="noopen" href="mentions/mangaland.php">MangaGaming</a></li>
        <li><a target="_blank" rel="noopen" href="mentions/politique_de_protection_de_la_vie_privée_-_modèle1.pdf">Données personnelles et Cookies</a></li>
        <li><a target="_blank" rel="noopen" href="error.php">Gérer mes cookies</a></li>
        <li><a target="_blank" rel="noopen" href="mentions/rgpd.php">Mentions légales</a></li>
    </ul>
<?php // echo $_SERVER['PHP_SELF'] ?>
</div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="../../assets/js/core/jquery.min.js" type="text/javascript"></script>
<script src="../../assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="../../assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
  
<script src="../../assets/js/material-kit.js?v=2.0.6" type="text/javascript"></script>

<script src="../../assets/js/search.js" type="text/javascript"></script>
</body>
</html>