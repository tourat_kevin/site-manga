<?php session_start(); ?>

<!DOCTYPE html>
<html lang="fr">

<head>
  <title>MangaGaming by Alex</title>
  <!-- Required meta tags -->
  <meta name="description" content="ceci est la meta description en cours de développement de Manda@land">
  <?php include 'header/stylesheet.php' ?>
</head>

<body id="haut">
<!-- <div id="progressbar"></div>
<div id="scrollPath"></div> -->
  <!-- =========================================================================================================================================================================== -->
  <!-- navbar -->

  <?php include 'header/navBar.php' ?>

  <!-- fin de nav  -->
  <div class="page-header clear-filter" data-parallax="false">
    <div class="container">
      <div class="row">
        <div class="col-md-8 ml-auto mr-auto">
          <div class="brand text-center">
            <h1>MangaGaming</h1>
            <h3 class="title text-center">Site en cours de développement</h3>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="container">
<div class="row">

<?php require 'anime.php' ?>

<div class="space-800"></div>
<?php
// Partie "Liens"
/* On calcule le nombre de pages */
// $nombreDePages = ceil($nombredElementsTotal / $limite);

/* Si on est sur la première page, on n'a pas besoin d'afficher de lien
 * vers la précédente. On va donc ne l'afficher que si on est sur une autre
 * page que la première */


/* On va effectuer une boucle autant de fois que l'on a de pages */
for ($i = 1; $i <= $nombreDePages; $i++):
     /*?><a href="?page=<?php echo $i; ?>"><?php echo $i; ?></a> <?php */
endfor;
?>

            </div>
        </div>
    </div>
</div>
<?php include 'footer.php' ?>

<script src="../assets/js/screen.js" type="text/javascript"></script>