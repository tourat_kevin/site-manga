<?php
// session_start();

require_once('../bdd.php');


if(isset($_GET['anime'])) {
    $anime = (string) trim($_GET['anime']);
    $anime = '%'.$anime.'%';
    $req = $dbh->prepare('SELECT `anime_id`, `name` FROM `anime` WHERE `name` LIKE :anime LIMIT 10');
    $req->execute(array(':anime'=>$anime));
    $req = $req->fetchALL();

    foreach($req as $r) {
        ?>
        <div style="margin-top: 20px 0; border-bottom: 2px solid red;">
            <a href="../pages/animeInfo.php?id=<?php echo $r['anime_id']?>"><?= $r['name']?></a>
        </div>
    <?php
    }
}