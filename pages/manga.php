<?php session_start(); ?>

<!DOCTYPE html>
<html lang="fr">

<head>
  <title>MangaGaming by Alex</title>
  <!-- Required meta tags -->
  <meta name="description" content="ceci est la meta description en cours de développement de Manda@land">
  <?php include 'header/stylesheet.php' ?>
</head>

<body id="haut">
<!-- <div id="progressbar"></div>
<div id="scrollPath"></div> -->
  <!-- =========================================================================================================================================================================== -->
  <!-- navbar -->

  <?php include 'header/navBar.php' ?>

  <!-- fin de nav  -->
  <div class="page-header clear-filter" data-parallax="false">
    <div class="container">
      <div class="row">
        <div class="col-md-8 ml-auto mr-auto">
          <div class="brand text-center">
            <h1>MangaGaming</h1>
            <h3 class="title text-center">Site en cours de développement</h3>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="container">
<div class="row">

<?php

require '../bdd.php';


$page = (!empty($_GET['page']) ? $_GET['page'] : 1);
$limite = 24;

// Partie "Requête"
/* On commence par récupérer le nombre d'éléments total. Comme c'est une requête,
 * il ne faut pas oublier qu'on ne récupère pas directement le nombre.
 * Ici, comme la requête ne contient aucune donnée client pour fonctionner,
 * on peut l'exécuter ainsi directement */
$resultFoundRows = $dbh->query('SELECT count(id) FROM `anime`');
/* On doit extraire le nombre du jeu de résultat */
$nombredElementsTotal = $resultFoundRows->fetchColumn();

$debut = ($page - 1) * $limite;
/* Cette requête ne change pas */
$query = 'SELECT id, name, rating, description, img FROM `anime` ORDER BY `name` LIMIT :limite OFFSET :debut';
$query = $dbh->prepare($query);
$query->bindValue('debut', $debut, PDO::PARAM_INT);
$query->bindValue('limite', $limite, PDO::PARAM_INT);
$query->execute();


// function GetAlphabetArtist($db) {
//   $listtoabbreviate=array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
//   $doneOther=0;
//   $sql=$dbh->prepare('SELECT DISTINCT id, name, rating, description, img FROM `anime` ORDER BY `name` ASC LIMIT :limite OFFSET :debut');
//   while ($list=$req->fetch(PDO::FETCH_ASSOC)) {
//       if(!in_array(strtoupper($list['name']),$listtoabbreviate)) {
//           if($doneOther==0) {
//               echo "<b> &nbsp;<a href=\\".$_SERVER['PHP_SELF']."?name=".$list['name'].">Other</A>&nbsp;</b>";
//               $doneOther==1;
//       } else {
//           echo "<b> &nbsp;<a href=\\".$_SERVER['PHP_SELF']."?name=".$list['name'].">".$list['name']."</A>&nbsp; </b>";
//       }
//   }
// }
// }


// $name = range('A', 'Z');
// if(preg_match('/\\A[a-z]\\Z/', $_GET['name'])) {
//   $query = 'SELECT id, name, rating, description, img FROM `anime` WHERE `name` LIKE `$name%` LIMIT :limite OFFSET :debut';
// } else {
//   $query = 'SELECT id, name, rating, description, img FROM `anime` ORDER BY `name` LIMIT :limite OFFSET :debut';
// }



 
$alphabet = array('0-9'); // ligne modifiée
for($i = 'A'; $i != 'AA'; $i++){
 $alphabet[] = $i;
}
if(isset($_GET['tri']) AND in_array($_GET['tri'], $alphabet)){
 $tri = $_GET['tri'];
 $tri == '0-9' ? $where = "REGEXP '^([0-9]+)'" : $where = "LIKE '$tri%'"; // ligne ajoutée
 $req = $dbh->prepare("SELECT `name` FROM `anime` WHERE id AND name $where");  // ligne modifiée
 while($dat = $req->fetch(PDO::FETCH_ASSOC)){
  echo $dat['name'].'<br />';
 }
}
else{
 foreach($alphabet as $list){
  echo '<a href="manga.php?tri='.$list.'">'.$list.'</a> ';
 }
}




// Partie "Boucle"
while ($e = $query->fetch()) {
    
  echo '<div class="col-md-4 col-card">
          <div class="card-container">
            <div class="card">
                <div class="front">
                  <div class="header-front">
                    <h3>'.$e['name'].'</h3>
                  </div>
                  <div class="space-30"></div>
                  <img src="../'.$e['img'].'?'.rand().'" alt="image-anime">
                </div>
                <div class="back">
                  <div class="header">
                      <h3 class="motto">'.$e['name'].'</h3>
                  </div>
                    <div class="content">
                        <div class="main">
                            <h4 class="text-center">Description</h4>
                            <p class="text-center">'.$e['description'].'</p>

                            <div class="stats-container">
                                <div class="stats">
                                    <h4>Vote</h4>
                                    <h4>'.$e['rating'].'</h4>
                                </div>
                                 <a class="btn btn-outline-info" href="animeInfo.php?id='.$e['id'].'">Voir ici</a>
                            </div>

                        </div>
                    </div>
                  
                </div>
              </div>
            </div>
          </div>';
          }        
?>
<div class="space-800"></div>
<?php
// Partie "Liens"
/* On calcule le nombre de pages */
$nombreDePages = ceil($nombredElementsTotal / $limite);

/* Si on est sur la première page, on n'a pas besoin d'afficher de lien
 * vers la précédente. On va donc ne l'afficher que si on est sur une autre
 * page que la première */
if ($page > 1):
    ?><a href="?page=<?php echo $page - 1; ?>">Page précédente</a> — <?php
endif;

/* On va effectuer une boucle autant de fois que l'on a de pages */
for ($i = 1; $i <= $nombreDePages; $i++):
     /*?><a href="?page=<?php echo $i; ?>"><?php echo $i; ?></a> <?php */
endfor;
?>



<?php
/* Avec le nombre total de pages, on peut aussi masquer le lien
 * vers la page suivante quand on est sur la dernière */
if ($page < $nombreDePages):
    ?>— <a href="?page=<?php echo $page + 1; ?>">Page suivante</a><?php
endif;
?>

            </div>
        </div>
    </div>
</div>
<?php include 'footer.php' ?>

<script src="../assets/js/screen.js" type="text/javascript"></script>