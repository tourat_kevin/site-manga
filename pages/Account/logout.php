<?php

session_start(); // active la session active depuis la connexion au site

session_unset(); // Désactive la session en cours et en crée une nouvelle

session_destroy(); //Détruit les variable en cours relative a la session, ainsi que la session utilisée lors de la navigation

//setcookie('authentification', '', time() -1); // On supprime le cookie


header('location: ../index.php');
exit();