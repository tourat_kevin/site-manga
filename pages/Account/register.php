<?php
    session_start();
    
    // require '../error.php';
			
	// if(isset($_SESSION['connect'])) {
	// 	header('location: index.php');
	// 	exit();
	// }
						
	if(!empty($_POST['pseudo']) && !empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['password_two'])) {
        require '../../bdd.php';
        $pseudo = $_POST['pseudo'];
		$email = $_POST['email'];
		$password = $_POST['password'];

		if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			header('location: register.php?error=1&message=Votre adresse email n\'est pas valide !');
			exit();
		}
		
		
		

		$request = $dbh->prepare("SELECT count(*) AS numberEmailExist FROM users WHERE email=?");
		$request->execute(array($email));
		while($email_verification = $request->fetch()) {
			if ($email_verification['numberEmailExist'] != 0) {
				// email existe
				header('location: register.php?error=1&message=Votre adresse email est déjà utilisée');
				exit();
			}
		}

		if($_POST['password'] == $_POST['password_two']) {
			// Enregistre
			// $passwordHash = password_hash($password, PASSWORD_DEFAULT);
			$secret = hash("sha256", $email, false).time();
			$password = hash("sha256", $password, false);

			
			$request = $dbh->prepare('INSERT INTO `users` (pseudo, email, password, secret) VALUE (?,?,?,?)');
			$request->execute(array($pseudo,$email,$password,$secret));
			header('location: register.php?success=1&message=ok');
			// echo 'ok';
		}
		else {
			echo 'mots de passes pas identiques';
		}
		
    }
?>


<!DOCTYPE html>
<html>
<head>
	<title>MangaGaming register</title>
	<meta name="description" content="ceci est la meta description en cours de développement de MandaGaming">
	<?php require '../header/stylesheet.php' ?>
	

	<!-- <link rel="stylesheet" type="text/css" href="design/default.css"> -->
	<!-- <link rel="icon" type="image/pngn" href="img/favicon.png"> -->
</head>
<body>
<?php require '../header/navBar.php' ?>
<div class="space-110"></div>
	<section class="bg-fff">
		<div id="login-body">
			<h1>S'inscrire</h1>
					<?php
						

						//RAJOUTER MESSAGES D'ERREUR

						if(isset($_GET['error'])) {
							if(isset($_GET['message'])) {
								echo '<div class="alert error">' . htmlspecialchars($_GET['message']). '</div>';
							} 
						} else if(isset($_GET['success'])) {
							echo '<div class="alert success">Vous êtes bien inscrit. <a href="login.php">Connectez-vous</a>. </div>';
						}
					?>

				<!-- FIN DU RAJOUT -->
			<form class="form-register" method="post" action="register.php">
                <input class="form-input" type="text" name="pseudo" placeholder="Votre speudo" require />
				<input class="form-input" type="email" name="email" placeholder="Votre adresse email" required />
				<input class="form-input" type="password" name="password" placeholder="Mot de passe" required />
				<input class="form-input" type="password" name="password_two" placeholder="Retapez votre mot de passe" required />
				<div class="space-30"></div>
				<button class="btn btn-outline-info" type="submit">S'inscrire</button>
			</form>

			<p class="grey">Déjà sur MangaGaming ? <a href="login.php">Connectez-vous</a>.</p>
		</div>
	</section>
</body>
</html>