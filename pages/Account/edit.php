<?php
session_start();
require '../../bdd.php';

if(isset($_SESSION['id'])) {
    $request = $dbh->prepare("SELECT * FROM `users` WHERE id = ?");
    $request->execute(array($_SESSION['id']));
    $userInfo = $request->fetch();

    if(isset($_POST['newpseudo']) && !empty($_POST['newpseudo']) && $_POST['newpseudo'] != $userInfo['pseudo']) {
        $newpseudo = htmlspecialchars($_POST['newpseudo']);
        $insert = $dbh->prepare("UPDATE `users` SET pseudo = ? WHERE id = ?");
        $insert->execute(array($newpseudo, $_SESSION['id']));
        header('location: profil.php?id='.$_SESSION['id']);
    } else {
        // header('location: edit.php?error=1&message=Error');
    }

    if(isset($_POST['newemail']) && !empty($_POST['newemail']) && $_POST['newemail'] != $userInfo['email']) {
        if(!filter_var($_POST['newemail'], FILTER_VALIDATE_EMAIL)) {
			header('location: edit.php?error=1&message=Votre adresse email n\'est pas valide !');
			exit();
		}
        $request = $dbh->prepare("SELECT count(*) AS numberEmailExist FROM users WHERE email=?");
        $request->execute(array($email));
        while($email_verification = $request->fetch()) {
            if ($email_verification['numberEmailExist'] != 0) {
                // email existe
                header('location: edit.php?error=1&message=Votre adresse email est déjà utilisée');
                exit();
            }
        }
        $newemail = htmlspecialchars($_POST['newemail']);
        $insert = $dbh->prepare("UPDATE `users` SET email = ? WHERE id = ?");
        $insert->execute(array($newemail, $_SESSION['id']));
        header('location: profil.php?id='.$_SESSION['id']);
    } else {
        // header('location: edit.php');
    }
    
    if(isset($_POST['newmdp1']) && !empty($_POST['newmdp1']) && isset($_POST['newmdp2']) && !empty($_POST['newmdp2'])) {
        $mdp1 = hash("sha256", $_POST['newmdp1'], false);
        $mdp2 = hash("sha256", $_POST['newmdp2'], false);

        if($mdp1 == $mdp2) {
            $insert = $dbh->prepare("UPDATE `users` SET password = ? WHERE id = ?");
            $insert->execute(array($mdp2, $_SESSION['id']));
            header('location: profil.php?id='.$_SESSION['id'].'&success=1&message=Vos modification à bien été pris en compte');
        } else if($mdp1 != $mdp2) {
            header('location: edit.php?error=1&message=Vos mot de passe ne corresponde pas !');
            // echo "<div class='alert error'>Vos mot de passe ne corresponde pas !</div>";
        } else {
            header('location: edit.php?error=1&message=Une erreur est survenue');
        }
    }
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>MangaGaming Profil</title>
	<?php require '../header/stylesheet.php' ?>

	<!-- <link rel="stylesheet" type="text/css" href="design/default.css">
	<link rel="icon" type="image/pngn" href="img/favicon.png"> -->
</head>
<body>
	<?php require '../header/navBar.php' ?>
	<div class="space-110"></div>

    <h2>Modifiication du profil de <?php echo $userInfo["pseudo"] ?></h2>
    <!-- GESTION MESSAGE ERROR -->
    <?php
        if(isset($_GET['error'])) {
            if(isset($_GET['message'])) {
                echo '<div class="alert error">'.htmlspecialchars($_GET['message']).'</div>';
            }
        }
    ?>
        <div class="container">
            <div class="row">
            <form class="text-center-item" method="POST" action="">
                <label class="text-left">Nouveau pseudo</label>
                <input class="form-input" type="text" name="newpseudo" placeholder="Nouveau pseudo" value="<?php echo $userInfo["pseudo"] ?>">
                <div class="space-20"></div>
                <label class="text-left">Nouveau mail</label>
                <input class="form-input" type="text" name="newemail" placeholder="Nouveau email" value="<?php echo $userInfo["email"] ?>">
                <div class="space-20"></div>
                <label class="text-left">Nouveau mot de passe</label>
                <input class="form-input" type="password" name="newmdp1" placeholder="Nouveau mot de passe">
                <div class="space-20"></div>
                <label class="text-left">Confirmer mot de passe</label>
                <input class="form-input" type="password" name="newmdp2" placeholder="Confirmer nouveau mot de passe">
                <div class="space-20"></div>
                <input class="btn btn-outline-info" type="submit" value="Mettre à jour mon profil !">
            </form>
            </div>
        </div>
        <div class="space"></div>
    <?php include('../footer.php'); ?>
</body>
</html>
<?php
} else {
    header ('location: edit.php?error=1&message=error');
}
?>