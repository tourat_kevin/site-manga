<?php
session_start();
require '../../bdd.php';

if(isset($_GET['id']) && $_GET['id'] > 0 ) {
    $getId = intval($_GET['id']);
    $request = $dbh->prepare('SELECT * FROM `users` WHERE id = ?');
    $request->execute(array($getId));
    $userInfo = $request->fetch();
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>MangaGaming Profil</title>
	<?php  require '../header/stylesheet.php' ?>

	<!-- <link rel="stylesheet" type="text/css" href="design/default.css">
	<link rel="icon" type="image/pngn" href="img/favicon.png"> -->
</head>
<body>
	<?php require '../header/navBar.php' ?>
	<div class="space"></div>
    <div class="container bg-fff text-center-item">
        
            <h2>Profil de <?php echo $userInfo['pseudo']; ?></h2>

            <!-- GESTION MESSAGE ERROR -->
            <?php
                if(isset($_GET['error'])) {
                    if(isset($_GET['message'])) {
                        echo '<div class="alert error">'.htmlspecialchars($_GET['message']).'</div>';
                    }
                }
            ?>

            <h4>Pseudo = <?php echo $userInfo['pseudo']; ?></h4>
            <br>
            <h4>Mail = <?php echo $userInfo['email']; ?></h4>
            <div class="space"></div>


            <?php
            if(isset($_SESSION['id']) && $userInfo['id'] == $_SESSION['id']) {
            ?>
            <a class="btn btn-outline-info" href="edit.php">Edit profil</a>
            <a class="btn btn-deco" href="logout.php">Déconnexion</a>
            <div class="space"></div>
            <?php
            }
            ?>
        
    </div>
    <?php include '../footer.php'; ?>
</body>
</html>
<?php
}
?>