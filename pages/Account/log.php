<?php

if(isset($_COOKIE['authentification']) && !isset($_SESSION['connect'])) {
    $secret = htmlspecialchars($_COOKIE['authentification']);

    // verifier que le code $secret soit associé à un compte dans la BDD

    require '../../bdd.php';
    $reqSecret = $dbh->prepare("SELECT count(*) AS secretNumberAccount FROM users WHERE secret = ?");
    $reqSecret->execute(array($secret));

    while($userSecret = $reqSecret->fetch()) {
        if($userSecret['secretNumberrAccount'] == 1) {
            $reqUser = $dbh->prepare("SELECT * FROM users WHERE secret = ?");
            $reqUser->execute(array($secret));

            while($userAccount = $reqUser->fetch()) {
                //récupération des variable de sessions our récupérer la dès que le cookie est vérifié et bien associé a un compte utilisateur.

                $_SESSION['connect'] = 1;
                $_SESSION['email'] = $userAccount['email'];
            }
        }
    }
}

if(isset($_SESSION['connect'])) {
    require '../../bdd.php';
    // si l'utilisateur est connecté, on vient récupérer toutes les information de notre utilisateur

    $reqUser = $dbh->prepare("SELECT * FROM users WHERE email = ?");
    $reqUser->execute(array($_SESSION['email']));

    while($userAccount = $reqUser->fetch()) {
        if($userAccount['blocked'] == 1) {
            header('location: logout.php');
            exit();
        }
    }
}