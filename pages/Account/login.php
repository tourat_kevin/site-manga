<?php 

session_start();
require 'log.php';

if(!empty($_POST['email']) && !empty($_POST['password'])) {
	require '../../bdd.php';

	$email = htmlspecialchars($_POST['email']);
	$password = htmlspecialchars($_POST['password']);

	if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		header('location: register.php?error=1&message=Votre adresse email n\'est pas valide !');
		exit();
	}

	$password = hash("sha256", $password, false);

	$request = $dbh->prepare("SELECT count(*) AS numberEmail FROM `users` WHERE email=?");
	$request->execute(array($email));
	while($email_verification = $request->fetch()) {
		if ($email_verification['numberEmail'] != 1) {
			// email existe
			header('location: login.php?error=1&message=Impossible de vous authentifier.');
			exit();
		}
	}
	
	//CONNEXION

	$request = $dbh->prepare('SELECT * FROM `users` WHERE email = ?');
	$request->execute(array($email));

	//tant qu'il y a une ligne a afficher, la stocker dans un tableau (ici on va l'appeler $user)

	while($user = $request->fetch()) {
		if($password == $user['password'] && $user['blocked'] == 0) {

			// On va venir se servir de la sesionn pour voir si l'utilisateurr est connecté ou non (oui = 1; non = 0)

			$_SESSION['connect'] = 1;
			$_SESSION['id'] = $user['id'];
			$_SESSION['pseudo'] = $user['pseudo'];
			$_SESSION['email'] = $user['email'];

			if(isset($_POST['auto'])) {
				setcookie('authentification', $user['secret'], time() + 3600 * 24);
			}




			header('location: profil.php?id='.$_SESSION['id'].'?success=1');
			exit();
		} else {
			header('location: login.php?error=1&message=Impossible de vous authentifier');
			exit();
		}
	}
}


?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>MangaGaming Login</title>
	<?php require '../header/stylesheet.php' ?>

	<!-- <link rel="stylesheet" type="text/css" href="design/default.css">
	<link rel="icon" type="image/pngn" href="img/favicon.png"> -->
</head>
<body>
	<?php require '../header/navBar.php' ?>
	<div class="space-110"></div>
	<section>
		<div id="login-body">

			<?php

				if(isset($_SESSION['connect'])) {?>
				<h1 class="bg-fff">Hello ! Vous êtes déjà connecté.</h1>

			
				<!-- <p>Qu'est ce que tu veux regarder ?</p> -->
				<a href="logout.php">Deconnexion</a>
			<?php } else {?>
				<!-- SI LA SESSION CONNECT N'EXITE PAS, J'AFFICHE LE RESTE, DONC LE FORMULAIRE, SINON JE LE CACHE -->
				<div class="space-30"></div>
				<div class="container bg-fff">
				
					<h1>S'identifier</h1>

					<!-- GESTION MESSAGE ERROR -->
					<?php

						if(isset($_GET['error'])) {
							if(isset($_GET['message'])) {
								echo '<div class="alert error">'.htmlspecialchars($_GET['message']).'</div>';
							}
						}
					?>
					<!-- FIN GESTION MESSAGE ERROR -->
					<div class="space-30"></div>
					<form class="form-register" method="post" action="login.php">
						<!-- <input class="form-input" type="text" name="speudo" placeholder="Votre speudonyme" require /> -->
						<input class="form-input" type="email" name="email" placeholder="Votre adresse email" required />
						<input class="form-input" type="password" name="password" placeholder="Mot de passe" required />
						<div class="space-30"></div>
						<button class=" btn btn-outline-info" type="submit">S'identifier</button><br><br>
						<label id="option">
							<input class="checkbox" type="checkbox" name="auto" checked /> Se souvenir de moi
						</label>
					</form>
				

					<p class="grey">Première visite sur MangaGaming ? <a href="register.php">Inscrivez-vous</a>.</p>
				<?php } ?>
			</div>
			
		</div>
	</section>

	<?php include('../footer.php'); ?>
</body>
</html>