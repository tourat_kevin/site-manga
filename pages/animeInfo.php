<?php

session_start();

?>

<head>
<title>Anime Info</title>
<!-- Required meta tags -->
<meta name="description" content="ceci est la meta description en cours de développement de MangaGaming">
<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<!--     Fonts and icons     -->
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
<!-- Material Kit CSS -->
<!-- <link href="assets/css/material-kit.css?v=2.0.6" rel="stylesheet" /> -->
<link href="../assets/css/material-kit-perso.min.css" rel="stylesheet" />
<link href="../assets/css/animeInfo.css" rel="stylesheet" />
</head>

<?php include_once('header/navBar.php'); ?>


<div class="page-header clear-filter">
    <div class="container">
        <div class="row">
            <div class="col-md-8 ml-auto mr-auto">
                <div class="brand text-center">
                <h1>MangaGaming</h1>
                <h3 class="title text-center">Site en cours de développement</h3>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

require '../bdd.php';

$id = $_GET['id'];

if(isset($id)) {
    if(isset($_POST['note']) && !empty($_POST['note'])) {
        
        $rating = $dbh->query('SELECT rating FROM anime WHERE id ="'.$id.'"');
        $rating = $rating->fetch();
        $ratingTotal = intval($rating[0]);
        // var_dump($rating);
        // var_dump($ratingTotal);

        $tabNote = array($_POST['note'], $ratingTotal);
        $somme = $ratingTotal;
        
        var_dump($tabNote);
        for($i = 0; $i<count($tabNote); $i++) {
            $somme = $somme + $tabNote[$i];
        }
        
        var_dump($somme);
        $moyenne = $somme/count($tabNote);
        var_dump($moyenne);

        $insert = $dbh->prepare("UPDATE `anime` SET rating = ? WHERE id = ?");
        $insert->execute(array($moyenne, $id));

        // echo 'la moyenne est de : '.$moyenne.'.';
    } else if(isset($_POST['submit'])) {
        if(empty($_POST['note'])) {
            echo '<div class="alert error" style="text-align: center;">Une erreur c\'est produite. Veuillez réessayer ultérieurement.</div>';
        }
    }
}



$request = $dbh->prepare('SELECT id, name, type, episodes, rating, description, img, link FROM `anime` WHERE id = ? LIMIT 1');
$request->execute(array($id));

while ($e = $request->fetch()) {

    echo    
    '<h3>'.$e['name'].'</h3>
    <div class="animeInfo">
        <div class="card-info">
            <div class="info-general">
                <img id="imgAnimeInfo" src="../'.$e['img'].'?'.rand().'" alt="image-anime">
            </div>
        </div>
        <div class="information">
            <h4>Format : '.$e['type'].'</h4>
            <h4>Nombre d\'épisode : '.$e['episodes'].'</h4>
            <div class="note">
                <h4>Vote : '.$e['rating'].'</h4>
                ';
                if(isset($_SESSION["connect"]) == 1) {
                    echo '<form method="POST">
                            <input class="form-input" type="number" name="note" min="0" max="10">
                            <input class="btn btn-outline-info" type="submit" value="submit" name="submit">
                        </form>';
                }
             echo '   
            </div>
            <a href="'.$e['link'].'"><h4>Lien streaming</h4></a>
        </div>
    </div>           
    <div class="synopsi">
        <h3 class="text-center">Description</h3>
        <p class="text-center">'.$e['description'].'</p>
        <div class="space-70"></div>
    </div>
    <div class="space-30">
        <h3>Contactez-nous</h3>
    </div>';
            } 
include 'footer.php'
?>