<?php

session_start();

?>

<head>
<title>Anime Info</title>
<!-- Required meta tags -->
<meta name="description" content="ceci est la meta description en cours de développement de MangaGaming">
<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<!--     Fonts and icons     -->
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
<!-- Material Kit CSS -->
<!-- <link href="assets/css/material-kit.css?v=2.0.6" rel="stylesheet" /> -->
<link href="../assets/css/material-kit-perso.min.css" rel="stylesheet" />
<link href="../assets/css/animeInfo.css" rel="stylesheet" />

<link href="test/style.css" rel="stylesheet" type="text/css">
<link href="test/reviews.css" rel="stylesheet" type="text/css">
</head>

<?php include_once('header/navBar.php'); ?>


<div class="page-header clear-filter">
    <div class="container">
        <div class="row">
            <div class="col-md-8 ml-auto mr-auto">
                <div class="brand text-center">
                <h1>MANG@ LAND</h1>
                <h3 class="title text-center">Site en cours de développement</h3>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

require '../bdd.php';


$id = $_GET['id'];

// Below function will convert datetime to time elapsed string.
function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);
    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;
    $string = array('y' => 'year', 'm' => 'month', 'w' => 'week', 'd' => 'day', 'h' => 'hour', 'i' => 'minute', 's' => 'second');
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }
    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}



// Page ID needs to exist, this is used to determine which reviews are for which page.
if (isset($id)) {
    if (isset($_POST['name'], $_POST['rating'], $_POST['content'])) {
        // Insert a new review (user submitted form)
        $stmt = $dbh->prepare('INSERT INTO reviews (page_id, name, content, rating, submit_date) VALUES (?,?,?,?,NOW())');
        $stmt->execute([$id, $_POST['name'], $_POST['content'], $_POST['rating']]);
        exit('Your review has been submitted!');
    }
    // Get all reviews by the Page ID ordered by the submit date
    $stmt = $dbh->prepare('SELECT * FROM reviews WHERE page_id = ? ORDER BY submit_date DESC');
    $stmt->execute(array($id));
    $reviews = $stmt->fetchAll(PDO::FETCH_ASSOC);
    // Get the overall rating and total amount of reviews
    $stmt = $dbh->prepare('SELECT AVG(rating) AS overall_rating, COUNT(*) AS total_reviews FROM reviews WHERE page_id = ?');
    $stmt->execute(array($id));
    $reviews_info = $stmt->fetch(PDO::FETCH_ASSOC);
} else {
    exit('Please provide the page ID.');
}






$request = $dbh->prepare('SELECT * FROM `review`
                        INNER JOIN `reviews` ON review.reviews_id = reviews.reviews_id
                        INNER JOIN `anime` ON anime.members = reviews.anime_member');
$request = $dbh->prepare('SELECT * FROM `anime` WHERE id = ? LIMIT 1');
// $request = $dbh->prepare('SELECT * FROM `anime` LEFT JOIN `reviews` ON anime.rating = reviews.rating WHERE id = ? LIMIT 1');
$request->execute(array($id));


while ($e = $request->fetch()) {

    echo    
    '<h3>'.$e['name'].'</h3>
    <div class="animeInfo">
        <div class="card-info">
            <div class="info-general">
                <img id="imgAnimeInfo" src="../'.$e['img'].'?'.rand().'" alt="image-anime">
            </div>
        </div>
        <div class="information">
            <h4>Format : '.$e['type'].'</h4>
            <h4>Nombre d\'épisode : '.$e['episodes'].'</h4>
            <div class="note">
                <h4>Vote : '.$e['rating'].'</h4>

                <div class="overall_rating">
                    <span class="num">'.number_format($reviews_info['overall_rating'], 1).'</span>
                    <span class="stars">'.str_repeat('&#9733;', round($reviews_info['overall_rating'])).'</span>
                </div>
                <a href="#" class="write_review_btn">Write Review</a>
                <div class="write_review">
                    <form>
                        <input name="name" type="text" placeholder="Your Name" required>
                        <input name="rating" type="number" min="1" max="10" placeholder="Rating (1-10)" required>
                        <textarea name="content" placeholder="Write your review here..." required></textarea>
                        <button type="submit">Submit Review</button>
                    </form>
                </div>
            </div>
            <a href="'.$e['link'].'"><h4>Lien streaming</h4></a>
        </div>
    </div>           
    <div class="synopsi">
        <h3 class="text-center">Description</h3>
        <p class="text-center">'.$e['description'].'</p>
        <div class="space-70"></div>
    </div>
    <div class="space-30"></div>';
            } 
?>


<div class="content home">
			<h2>Reviews</h2>
			<p>Check out the below reviews for our website.</p>
            <div class="reviews"></div>
                <script>
                const reviews_page_id = 1;
                fetch("reviews.php?page_id=" + reviews_page_id).then(response => response.text()).then(data => {
                    document.querySelector(".reviews").innerHTML = data;
                    document.querySelector(".reviews .write_review_btn").onclick = event => {
                        event.preventDefault();
                        document.querySelector(".reviews .write_review").style.display = 'block';
                        document.querySelector(".reviews .write_review input[name='name']").focus();
                    };
                    document.querySelector(".reviews .write_review form").onsubmit = event => {
                        event.preventDefault();
                        fetch("reviews.php?page_id=" + reviews_page_id, {
                            method: 'POST',
                            body: new FormData(document.querySelector(".reviews .write_review form"))
                        }).then(response => response.text()).then(data => {
                            document.querySelector(".reviews .write_review").innerHTML = data;
                        });
                    };
                });
                </script>
		</div>



        <?php foreach ($reviews as $review): ?>
            <div class="review">
                <h3 class="name"><?=htmlspecialchars($review['name'], ENT_QUOTES)?></h3>
                <div>
                    <span class="rating"><?=str_repeat('&#9733;', $review['rating'])?></span>
                    <span class="date"><?=time_elapsed_string($review['submit_date'])?></span>
                </div>
                <p class="content"><?=htmlspecialchars($review['content'], ENT_QUOTES)?></p>
            </div>
        <?php endforeach ?>





    <div class="space-30">
        <h3>Contactez-nous</h3>
    </div>

<?=include 'footer.php'?>