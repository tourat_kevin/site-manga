<?php

try{
    $dbh = new PDO('mysql:host=localhost;dbname=opxafmmalexandre;charset=utf8', 'root', '');
    //On définit le mode d'erreur de PDO sur Exception
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // echo 'Connexion réussie';
}

/*On capture les exceptions si une exception est lancée et on affiche
les informations relatives à celle-ci*/
catch(PDOException $e){
echo "Erreur : " . $e->getMessage();
}