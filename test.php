<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
<!-- Material Kit CSS -->
<link href="assets/css/material-kit.css?v=2.0.6" rel="stylesheet" />
<link href="assets/css/main.css" rel="stylesheet">





<!DOCTYPE html>
<html lang="fr">

<head>
  <title>Mang@Land by Alex</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- Material Kit CSS -->
  <link href="assets/css/material-kit.css?v=2.0.6" rel="stylesheet" />
  <link href="assets/css/main.css" rel="stylesheet">
</head>

<body id="haut">
  <!-- =========================================================================================================================================================================== -->
  <!-- navbar -->

  <nav class="navbar navbar-img navbar-transparent navbar-color-on-scroll fixed-top navbar-expand-lg" color-on-scroll="15" id="sectionsNav">
    <div class="container">
        <div class="navbar-translate ml-auto">
            <a class="navbar-brand yes-mobile" href="#">
            <img src="image/logo-pc2.png" alt="logo-pc" class="img nav-gif no-mobile img-nav">           
            </a>            
            <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="navbar-nav ml-auto">
                               
                <li class="nav-item dropdown">
                    <!-- liste menu -->
                    <li class="nav-item">
                        <a class="nav-link" href="demo.html">
                            Manga
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                          Animé
                        </a>
                    </li>                   
                    <li class="nav-item">
                        <a class="nav-link" href="#">  
                           Jeux
                        </a>
                    </li>                   
                </li>
            </ul>
                    <!-- ma barre de recherche -->
            <ul class="navbar-nav ml-auto">
              <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-5" type="search" placeholder="Search" aria-label="Search" style="color: #fff !important;">
                <button class="btn btn-search" type="submit">
                  <i class="material-icons">search</i>
                  Search
                </button>
              </form>
            </ul>    
            <ul class="navbar-nav ml-auto">
                    <!-- ma nav déco / reco -->
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <i class="material-icons">login</i>
                  Login
                </a>           
              </li>
              <li class="nav-item">
                <a id="deco" class="nav-link" href="#">
                  <i class="material-icons">power_settings_new</i>
                </a>           
              </li>
            </ul>        
        </div>
    </div>
</nav>
  <!-- fin de nav  -->
  <div class="page-header clear-filter" data-parallax="true">
    <div class="container">
      <div class="row">
        <div class="col-md-8 ml-auto mr-auto">
          <div class="brand text-center">
            <!-- <h1>Your title here</h1>
            <h3 class="title text-center">Subtitle</h3> -->
          </div>
        </div>
      </div>
    </div>
  </div>


















<?php
$dbh = new PDO('mysql:host=localhost;dbname=anime;charset=utf8', 'root', '');

// $query = 'SELECT name, type FROM tbl_name';
// $result = $dbh->query($query, PDO::FETCH_ASSOC);
            // if ($result) {
            //     echo "<table>\n<tr><th>Name</th><th>Type</th></tr>";
            //     foreach($result as $row) {
            //         echo "<tr><td>".$row['name']."</td><td>".$row['type']."</td></tr>\n";
            //     }
            //     echo "</table>\n";       
            // }




?>

<?php
    // $imgageGlob = glob('image/*jpg');
    // print_r(pathinfo($imgageGlob[0]));
?>
<div class="container">
                <div class="row">
          <?php

            $query = 'SELECT anime_id, name, rating, description, links FROM manga ORDER BY `name` LIMIT 2 ';
            $result = $dbh->query($query, PDO::FETCH_ASSOC);
            foreach($result as $row) {
                
                // echo '1 '.$row['name'].' 2 '.$row['rating'].' 3';
                echo '
                    <div class="col-sm">

                        <div class="card-container">
                            <div class="card">
                                <div class="front">
                                    <h3>'.$row['name'].'</h3>
                                            <img src="'.$row['img'].'">


                                </div>
                                <div class="back">
                                    <div class="header">
                                        <h3 class="motto">'.$row['name'].'</h3>
                                    </div>
                                    <div class="content">
                                        <div class=main">
                                            <h4 class="text-center">Description</h4>
                                            <p class="text-center">'.$row['description'].'</p>

                                            <div class="stats-container">
                                                <div class="stats">
                                                    <h4>Vote</h4>
                                                    <h4>'.$row['rating'].'</h4>

                                                </div>
                                                
                                                    <a class="btn btn-outline-info" target="_blank" rel="noopener" href="'.$row['links'].'">Voir ici</a>

                                            </div>

                                        </div>
                                    </div>

                                </div>
                                </div>
                            </div>
                            </div>';
                          }
                          ?>
                          </div>
        </div>
    </div>
</div>

<script src="assets/js/core/jquery.min.js" type="text/javascript"></script>
<script src="assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
  
<script src="assets/js/material-kit.js?v=2.0.6" type="text/javascript"></script>