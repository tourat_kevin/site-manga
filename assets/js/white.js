window.onload = () => {
    let themeLink = document.getElementById('theme-link')

    if(localStorage.theme != null) {
        themeLink.href = `css/${localStorage.theme}.css`
    } else {
        themeLink.href = "css/mainV2.css"
        localStorage.theme = "sombre"
    }
}