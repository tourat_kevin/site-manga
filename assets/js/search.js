$(document).ready(function(){
    $('#search').keyup(function(){
      $('#result-search').html("");


      let utilisateur = $(this).val();
      if(utilisateur != "") {
        $.ajax({
          type: 'GET',
          url: '../pages/research.php',
          data: 'anime=' + encodeURIComponent(utilisateur),
          success: function(data) {
            if(data != "") {
              $('#result-search').append(data);
            } else {
              document.getElementById('result-search').innerHTML = '<div style="text-align: center; margin-top: 20px;">Aucun résultat</div>';
            }
          }
        });
      }
    });
});